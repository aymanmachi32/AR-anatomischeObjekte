using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
// Diese Klasse ist für das Spielen des Lunge-Quiz zuständig
public class LungeQuizScript : MonoBehaviour
{
    public TextMeshProUGUI largeText;

    private List<string> list = new List<string>(); // Ein dynamsiches Array wird erstellt
    private string RandomName;

    public GameObject GrenzezwischenOberundUnterlappenRechtsSpot;
    public GameObject KehlkopfSpot;
    public GameObject linkerUnterlappenSpot;
    public GameObject GrenzezwischenOberundUnterlappenLinksSpot;
    public GameObject rechterMittellappenSpot;
    public GameObject RechterLungenflügelSpot;
    public GameObject LuftröhreSpot;
    public GameObject LinkerLungenflügelSpot;
    public GameObject rechterUnterlappenSpot;
    public GameObject LinkerOberlappenSpot;
    public GameObject rechterOberlappenSpot;

    private bool GrenzezwischenOberundUnterlappenRechtsSet = true;
    private bool KehlkopfSet = true;
    private bool linkerUnterlappenSet = true;
    private bool GrenzezwischenOberundUnterlappenLinksSet = true;
    private bool rechterMittellappenSet = true;
    private bool RechterLungenflügelSet = true;
    private bool LuftröhreSet = true;
    private bool LinkerLungenflügelSet = true;
    private bool rechterUnterlappenSet = true;
    private bool LinkerOberlappenSet = true;
    private bool rechterOberlappenSet = true;


    public GameObject TextPanel;
    public GameObject FinalTextPanel;
    public TextMeshProUGUI finalText;
    public GameObject imgWellDone;
    public GameObject imgAverage;
    public GameObject imgNotGood;
    private int counter=0; // Zählt die Anzahl an Falschen Betätigungen

    private float time = 0.5f; // Zeit in dem das Falsche Pin die Farbe wechseln soll
    private float repeatRate;

    public AudioSource wellDoneSound;
    public AudioSource AverageSound;
    public AudioSource NotGoodSound;

    public AudioSource CorrectPin;
    public AudioSource WrongPin;
    // Diese Methode wird einmalig bei Start des Quizes ausgeführt.
    private void Start(){
        ResetTheQuiz(); // Es wird die Methode Reset Quiz aufgerufen
    }
    // Die Update Methode wird bei jedem Frame durchlaufen
    private void Update(){
        // Hier wird geschaut, ob eine Berührung auf dem Display vorliegt
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began))
        {
        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position); // Hier wird die Position der Berührung festgelegt
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.CompareTag(GrenzezwischenOberundUnterlappenRechtsSpot.tag) && GrenzezwischenOberundUnterlappenRechtsSet) // Hier wird geprüft, ob der Pin des anatomische Teil angetippt wurde und ob dieser Pin schon vorher auf grün gesetzt worden ist
            {
                if(RandomName == "die rechte Grenze zwischen Ober- und Unterlappen" && GrenzezwischenOberundUnterlappenRechtsSet){ // Wenn das anatomisch Teil das jenige ist, welches gesuscht wurde und dies zuvor nicht dran kamm, wird der Body durchlaufen
                    GrenzezwischenOberundUnterlappenRechtsSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0); // Wechseln der Frabe auf Grün
                    list.Remove(RandomName); // Anatomische Teil aus der Array List entfernen
                    GrenzezwischenOberundUnterlappenRechtsSet = false; // anatomische Teil wurde einmal korrekt angetippt, dadurch kann die Farbe nicht beim erneuten tippen auf rot wechseln
                    PickRandomFromList(); // Methode zu erneuten entnahme eines Elments aus der Array List
                    CorrectPin.Play(); // Sound für die korrekte Antwort wird abgespielt
                }else{
                    GrenzezwischenOberundUnterlappenRechtsSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0); // Wechseln der Frabe auf Rote
                    InvokeRepeating("ChangeColourGrenzezwischenOberundUnterlappenRechts", time, repeatRate); // Methode aufrufen die nach einer 0.5s die Farbe auf die Standard farbe zurücksetzt
                    WrongPin.Play(); // Sound für die falsche Antwort wird abgespielt
                    counter++; // Zähler wird um eins erhäht, da die Antwort falsch war
                }
            }

            if (hit.collider.CompareTag(KehlkopfSpot.tag) && KehlkopfSet)
            {
                if(RandomName == "der Kehlkopf" && KehlkopfSet){
                    KehlkopfSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    KehlkopfSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    KehlkopfSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourKehlkopf", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            }
            
            if (hit.collider.CompareTag(linkerUnterlappenSpot.tag) && linkerUnterlappenSet)
            {
                if(RandomName == "der linke Unterlappen" && linkerUnterlappenSet){
                    linkerUnterlappenSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    linkerUnterlappenSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    linkerUnterlappenSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourlinkerUnterlappen", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            } 

            if (hit.collider.CompareTag(GrenzezwischenOberundUnterlappenLinksSpot.tag) && GrenzezwischenOberundUnterlappenLinksSet)
            {
                if(RandomName == "die linke Grenze zwischen Ober- und Unterlappen" && GrenzezwischenOberundUnterlappenLinksSet){
                    GrenzezwischenOberundUnterlappenLinksSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    GrenzezwischenOberundUnterlappenLinksSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    GrenzezwischenOberundUnterlappenLinksSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourGrenzezwischenOberundUnterlappenLinks", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            }

            if (hit.collider.CompareTag(rechterMittellappenSpot.tag) && rechterMittellappenSet)
            {
                if(RandomName == "der rechte Mittellappen" && rechterMittellappenSet){
                    rechterMittellappenSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    rechterMittellappenSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    rechterMittellappenSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourrechterMittellappen", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            } 
            if (hit.collider.CompareTag(RechterLungenflügelSpot.tag) && RechterLungenflügelSet)
            {
                if(RandomName == "der rechte Lungenflügel" && RechterLungenflügelSet){
                    RechterLungenflügelSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    RechterLungenflügelSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    RechterLungenflügelSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourRechterLungenflügel", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            } 
            if (hit.collider.CompareTag(LuftröhreSpot.tag) && LuftröhreSet)
            {
                if(RandomName == "die Luftröhre" && LuftröhreSet){
                    LuftröhreSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    LuftröhreSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    LuftröhreSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourLuftröhre", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            } 
            if (hit.collider.CompareTag(LinkerLungenflügelSpot.tag) && LinkerLungenflügelSet)
            {
                if(RandomName == "der linke Lungenflügel" && LinkerLungenflügelSet){
                    LinkerLungenflügelSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    LinkerLungenflügelSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    LinkerLungenflügelSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourLinkerLungenflügel", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            } 
            if (hit.collider.CompareTag(rechterUnterlappenSpot.tag) && rechterUnterlappenSet)
            {
                if(RandomName == "der rechte Unterlappen" && rechterUnterlappenSet){
                    rechterUnterlappenSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    rechterUnterlappenSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    rechterUnterlappenSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourrechterUnterlappen", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            } 
            if (hit.collider.CompareTag(LinkerOberlappenSpot.tag) && LinkerOberlappenSet)
            {
                if(RandomName == "der linke Oberlappen" && LinkerOberlappenSet){
                    LinkerOberlappenSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    LinkerOberlappenSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    LinkerOberlappenSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourLinkerOberlappen", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            } 

            if (hit.collider.CompareTag(rechterOberlappenSpot.tag) && rechterOberlappenSet)
            {
                if(RandomName == "der rechte Oberlappen" && rechterOberlappenSet){
                    rechterOberlappenSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    rechterOberlappenSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    rechterOberlappenSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourrechterOberlappen", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            }
        
        }
        }
        }
    // Hier sind die Methoden, die dafür sorgen das nach 0.5s auf die Standard Farbe zurückgesetzt wird
    private void ChangeColourGrenzezwischenOberundUnterlappenRechts()
    { 
        GrenzezwischenOberundUnterlappenRechtsSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourKehlkopf()
    { 
        KehlkopfSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourlinkerUnterlappen()
    { 
        linkerUnterlappenSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourGrenzezwischenOberundUnterlappenLinks()
    { 
        GrenzezwischenOberundUnterlappenLinksSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourrechterMittellappen()
    { 
        rechterMittellappenSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
     private void ChangeColourRechterLungenflügel()
    { 
        RechterLungenflügelSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourLuftröhre()
    { 
        LuftröhreSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourLinkerLungenflügel()
    { 
        LinkerLungenflügelSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourrechterUnterlappen()
    { 
        rechterUnterlappenSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourLinkerOberlappen()
    { 
        LinkerOberlappenSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourrechterOberlappen()
    { 
        rechterOberlappenSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }

    // Dieses Methode sorgt für die Beschriftung im Quiz-Panel
    public void BtnAction(){
        PickRandomFromList(); // Hier wird die PickRandomFromList aufgrufen
    } 

    // Diese Methode ist für den willkürlichen Text der eingeblendet wird
    private void PickRandomFromList(){
        string textAmAnfang = "Wo ist "; // Anfangstext für die Frage
        string textAmEnde = "?"; // Fragezeichen für das Textende
        if(list.Count == 0){ // Prüft, ob die Array List leer ist
            TextPanel.SetActive(false); // Text-Panel ausgelendet
            FinalTextPanel.SetActive(true); // End-Panel einblenden
            imgWellDone.SetActive(false);
            imgAverage.SetActive(false);
            imgNotGood.SetActive(false);
            double ergCounter = (double) 100 - counter*4.55; // Die Punktzahl berechnen
            if(ergCounter<=0){ // Prüfen das es keine negativen Punkte gibt
                ergCounter=0;
            }
            if(ergCounter <= 100 && ergCounter >= 90.9){ // Wenn die erreichte Punktzahl zwischen 100 und 90.9 liegt
                imgWellDone.SetActive(true); // Es wird ein Bild eingeblendet, welches dem Nutzer mitteil, dass diese eine gute Leitung war
                wellDoneSound.Play(); // sehr gute Leistung Sound wird abgespielt
                finalText.text = "Gut gemacht. Weiter So!!!. Du hast " + ergCounter + "% erreicht."; // Text wird eingeblenden mit den erreichten Punkte
            }else{
                if(ergCounter <= 90.8 && ergCounter >= 72.7){ // Wenn die erreichte Punktzahl zwischen 90.8 und 72.7 liegt
                    imgAverage.SetActive(true); // Es wird ein Bild eingeblendet, welches dem Nutzer mitteil, dass diese eine durchschnittliche Leitung war
                    AverageSound.Play(); // durchschnittliche Leistung Sound wird abgespielt
                    finalText.text = "Übe noch ein bisschen. Du hast " + ergCounter + "% erreicht."; // Text wird eingeblenden mit den erreichten Punkte
                }else{ // Wenn die erreichte Punktzahl unterhalb von 72.7 liegt
                    imgNotGood.SetActive(true); // Es wird ein Bild eingeblendet, welches dem Nutzer mitteil, dass diese eine schlechte Leitung war
                    NotGoodSound.Play(); // schlechte Leistung Sound wird abgespielt
                    finalText.text = "Übeung mach den Meister. Du hast " + ergCounter + "% erreicht."; // Text wird eingeblenden mit den erreichten Punkte
                }
            }
            
        }else{
            RandomName = list[Random.Range(0, list.Count)]; // Aus der Array List wird ein willkürliches anatomische Teil gewählt
            largeText.text = textAmAnfang + RandomName + textAmEnde; // Hier wird dann der Text dem den Nutzer eingebelendet wird
        }
    }
    // Diese Methode wird bei Start ausgeführt, damit alle Einstellungen zurückgesetzt werden können
    public void ResetTheQuiz(){
        wellDoneSound.Stop(); // Sehr Gute Leistung Sound abstellen, falls dies aktiv ist
        AverageSound.Stop(); // Durchschnittliche Leistung Sound abstellen, falls dies aktiv ist
        NotGoodSound.Stop(); // nicht gute Leistung Sound abstellen, falls dies aktiv ist
        FinalTextPanel.SetActive(false); // Final Panel ausblenden, falls dies aktiv ist
        TextPanel.SetActive(true); // Text-Panel einblenden
        list.Clear();  // Array List leeren, falls Spiel abgebrochen wird
        counter = 0; // Zähler zurücksetzten
        
        list.Add("die rechte Grenze zwischen Ober- und Unterlappen"); // Befüllung der anatmoischen Teile in die Array List
        list.Add("der Kehlkopf");
        list.Add("der linke Unterlappen");
        list.Add("die linke Grenze zwischen Ober- und Unterlappen");
        list.Add("der rechte Mittellappen");
        list.Add("der rechte Lungenflügel");
        list.Add("die Luftröhre");
        list.Add("der linke Lungenflügel");
        list.Add("der rechte Unterlappen");
        list.Add("der linke Oberlappen");
        list.Add("der rechte Oberlappen");

        GrenzezwischenOberundUnterlappenRechtsSet = true; // Ist für die Überprüfung, ob ein Pin schonmal Korrekt bestimmt wurde
        KehlkopfSet = true;
        linkerUnterlappenSet = true;
        GrenzezwischenOberundUnterlappenLinksSet = true;
        rechterMittellappenSet = true;
        RechterLungenflügelSet = true;
        LuftröhreSet = true;
        LinkerLungenflügelSet = true;
        rechterUnterlappenSet = true;
        LinkerOberlappenSet = true;
        rechterOberlappenSet = true;


        GrenzezwischenOberundUnterlappenRechtsSpot.GetComponent<Renderer>().material.color = new Color(250,250,250); // Farben der Pins auf weiß zurücksetzten
        KehlkopfSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        linkerUnterlappenSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        GrenzezwischenOberundUnterlappenLinksSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        rechterMittellappenSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        RechterLungenflügelSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        LuftröhreSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        LinkerLungenflügelSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        rechterUnterlappenSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        LinkerOberlappenSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        rechterOberlappenSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
    }
}
