using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
// Diese Klasse ist für das Spielen des Gehirn-Quiz zuständig
public class GehirnQuizScript : MonoBehaviour
{
    public TextMeshProUGUI largeText;

    private List<string> list = new List<string>(); // Ein dynamsiches Array wird erstellt
    private string RandomName;

   
    public GameObject KleinhirnSpot;
    public GameObject StirnhirnSpot;
    public GameObject HirnstammSpot;
    public GameObject ScheitellappenSpot;
    public GameObject OkziptiallappenSpot;

    private bool KleinhirnSet = true;
    private bool StirnhirnSet = true;
    private bool HirnstammSet = true;
    private bool ScheitellappenSet = true;
    private bool OkziptiallappenSet = true;

    private float time = 0.5f; // Zeit in dem das Falsche Pin die Farbe wechseln soll
    private float repeatRate;

    public GameObject TextPanel;
    public GameObject FinalTextPanel;
    public TextMeshProUGUI finalText;
    public GameObject imgWellDone;
    public GameObject imgAverage;
    public GameObject imgNotGood;
    private int counter = 0; // Zählt die Anzahl an Falschen Betätigungen

    public AudioSource wellDoneSound;
    public AudioSource AverageSound;
    public AudioSource NotGoodSound;

    public AudioSource CorrectPin;
    public AudioSource WrongPin;
    // Diese Methode wird einmalig bei Start des Quizes ausgeführt.
    private void Start(){
        ResetTheQuiz(); // Es wird die Methode Reset Quiz aufgerufen
    }
    // Die Update Methode wird bei jedem Frame durchlaufen
    private void Update(){
        // Hier wird geschaut, ob eine Berührung auf dem Display vorliegt
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began))
        {
        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position); // Hier wird die Position der Berührung festgelegt
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.CompareTag(KleinhirnSpot.tag) && KleinhirnSet) // Hier wird geprüft, ob der Pin des anatomische Teil angetippt wurde und ob dieser Pin schon vorher auf grün gesetzt worden ist
            {
                if(RandomName == "das Kleinhirn" && KleinhirnSet){ // Wenn das anatomisch Teil das jenige ist, welches gesuscht wurde und dies zuvor nicht dran kamm, wird der Body durchlaufen
                    KleinhirnSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0); // Wechseln der Frabe auf Grün
                    list.Remove(RandomName); // Anatomische Teil aus der Array List entfernen
                    KleinhirnSet = false; // anatomische Teil wurde einmal korrekt angetippt, dadurch kann die Farbe nicht beim erneuten tippen auf rot wechseln
                    PickRandomFromList(); // Methode zu erneuten entnahme eines Elments aus der Array List
                    CorrectPin.Play(); // Sound für die korrekte Antwort wird abgespielt
                }else{
                    KleinhirnSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0); // Wechseln der Frabe auf Rote
                    InvokeRepeating("ChangeColourKleinhirn", time, repeatRate); // Methode aufrufen die nach einer 0.5s die Farbe auf die Standard farbe zurücksetzt
                    WrongPin.Play(); // Sound für die falsche Antwort wird abgespielt
                    counter++; // Zähler wird um eins erhäht, da die Antwort falsch war
                }
            }

            if (hit.collider.CompareTag(StirnhirnSpot.tag) && StirnhirnSet)
            {
                if(RandomName == "das Stirnhirn" && StirnhirnSet){
                    StirnhirnSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    StirnhirnSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    StirnhirnSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourStrinhirn", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            }
            
            if (hit.collider.CompareTag(HirnstammSpot.tag) && HirnstammSet)
            {
                if(RandomName == "der Hirnstamm" && HirnstammSet){
                    HirnstammSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    HirnstammSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    HirnstammSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourHirnstamm", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            } 

            if (hit.collider.CompareTag(ScheitellappenSpot.tag) && ScheitellappenSet)
            {
                if(RandomName == "der Scheitellappen" && ScheitellappenSet){
                    ScheitellappenSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    ScheitellappenSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    ScheitellappenSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourScheitellappen", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            }

            if (hit.collider.CompareTag(OkziptiallappenSpot.tag) && OkziptiallappenSet)
            {
                if(RandomName == "der Okziptiallappen" && OkziptiallappenSet){
                    OkziptiallappenSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    OkziptiallappenSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    OkziptiallappenSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourOkziptiallappen", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            } 
        
        }
        }
        }
    // Hier sind die Methoden, die dafür sorgen das nach 0.5s auf die Standard Farbe zurückgesetzt wird
    private void ChangeColourKleinhirn()
    { 
        KleinhirnSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourStrinhirn()
    { 
        StirnhirnSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourHirnstamm()
    { 
        HirnstammSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourScheitellappen()
    { 
        ScheitellappenSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourOkziptiallappen()
    { 
        OkziptiallappenSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }

    // Dieses Methode sorgt für die Beschriftung im Quiz-Panel
    public void BtnAction(){
        PickRandomFromList(); // Hier wird die PickRandomFromList aufgrufen
    } 

    // Diese Methode ist für den willkürlichen Text der eingeblendet wird
    private void PickRandomFromList(){
        string textAmAnfang = "Wo ist "; // Anfangstext für die Frage
        string textAmEnde = "?"; // Fragezeichen für das Textende
        if(list.Count == 0){ // Prüft, ob die Array List leer ist
            TextPanel.SetActive(false); // Text-Panel ausgelendet
            FinalTextPanel.SetActive(true); // End-Panel einblenden
            imgWellDone.SetActive(false);
            imgAverage.SetActive(false);
            imgNotGood.SetActive(false);
            double ergCounter = (double) 100 - counter * 10.0; // Die Punktzahl berechnen
            if(ergCounter<=0){ // Prüfen das es keine negativen Punkte gibt
                ergCounter=0;
            }
            if(ergCounter <= 100 && ergCounter >= 80){ // Wenn die erreichte Punktzahl zwischen 100 und 80 liegt
                imgWellDone.SetActive(true); // Es wird ein Bild eingeblendet, welches dem Nutzer mitteil, dass diese eine gute Leitung war
                wellDoneSound.Play(); // sehr gute Leistung Sound wird abgespielt
                finalText.text = "Gut gemacht. Weiter So!!!. Du hast " + ergCounter + "% erreicht."; // Text wird eingeblenden mit den erreichten Punkte
            }else{ 
                if(ergCounter <= 89 && ergCounter >= 60){ // Wenn die erreichte Punktzahl zwischen 89 und 60 liegt
                    imgAverage.SetActive(true); // Es wird ein Bild eingeblendet, welches dem Nutzer mitteil, dass diese eine durchschnittliche Leitung war
                    AverageSound.Play(); // durchschnittliche Leistung Sound wird abgespielt
                    finalText.text = "Übe noch ein bisschen. Du hast " + ergCounter + "% erreicht."; // Text wird eingeblenden mit den erreichten Punkte
                }else{ // Wenn die erreichte Punktzahl unterhalb von 60 liegt
                    imgNotGood.SetActive(true); // Es wird ein Bild eingeblendet, welches dem Nutzer mitteil, dass diese eine schlechte Leitung war
                    NotGoodSound.Play(); // schlechte Leistung Sound wird abgespielt
                    finalText.text = "Übeung mach den Meister. Du hast " + ergCounter + "% erreicht."; // Text wird eingeblenden mit den erreichten Punkte
                }
            }
            
        }else{
            RandomName = list[Random.Range(0, list.Count)]; // Aus der Array List wird ein willkürliches anatomische Teil gewählt
            largeText.text = textAmAnfang + RandomName + textAmEnde; // Hier wird dann der Text dem den Nutzer eingebelendet wird
        }
    }
    // Diese Methode wird bei Start ausgeführt, damit alle Einstellungen zurückgesetzt werden können
    public void ResetTheQuiz(){
        wellDoneSound.Stop(); // Sehr Gute Leistung Sound abstellen, falls dies aktiv ist
        AverageSound.Stop(); // Durchschnittliche Leistung Sound abstellen, falls dies aktiv ist
        NotGoodSound.Stop(); // nicht gute Leistung Sound abstellen, falls dies aktiv ist
        FinalTextPanel.SetActive(false); // Final Panel ausblenden, falls dies aktiv ist
        TextPanel.SetActive(true); // Text-Panel einblenden
        list.Clear(); // Array List leeren, falls Spiel abgebrochen wird
        counter = 0; // Zähler zurücksetzten
        
        list.Add("das Kleinhirn"); // Befüllung der anatmoischen Teile in die Array List
        list.Add("das Stirnhirn");
        list.Add("der Hirnstamm");
        list.Add("der Scheitellappen");
        list.Add("der Okziptiallappen");

        KleinhirnSet = true; // Ist für die Überprüfung, ob ein Pin schonmal Korrekt bestimmt wurde
        StirnhirnSet = true;
        HirnstammSet = true;
        ScheitellappenSet = true;
        OkziptiallappenSet = true;


        KleinhirnSpot.GetComponent<Renderer>().material.color = new Color(250,250,250); // Farben der Pins auf weiß zurücksetzten
        StirnhirnSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        HirnstammSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        ScheitellappenSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        OkziptiallappenSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
     
    }
}
