using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
// Diese Klasse ist für das wechseln der Modi im Hauptmenü der Herz zuständig
public class MenuScriptHerz : MonoBehaviour
{
    public GameObject PanelQuiz;
    public GameObject PanelAll;

    public GameObject Herz3D;
    public GameObject HerzAufbau3D;
    public GameObject HerzInnenbau3D;
    public GameObject HerzQuiz3D;
    public GameObject HerzAnim;

    public GameObject finalQuizPanel;

    public AudioSource herzschlagSound;
    public AudioSource buttonClick;
    private Animator Anim;


    // Über diese Methode werden die Allgemeinen Informationen eingeblendet
    public void HerzAllgemeineInfo()
    {
        
            // Hier wird Quiz-Text-Panel ausgeblendet, wenn dieser Aktiv ist
            if(CheckPanelOpenQuiz()){
                Animator animator = PanelQuiz.GetComponent<Animator>();
                animator.SetBool("open", false);
            
            }
            // Wenn das allgemeine Informationspanel, nicht offen ist soll die Animation abgespielt werden
            if (!CheckPanelOpenAllgemeineInfo())
            {
                
                Animator animatorAll = PanelAll.GetComponent<Animator>();
                animatorAll.SetBool("open", true); // Animation abspielen
                PanelQuiz.SetActive(false); // Quiz-Panel ausblenden
                finalQuizPanel.SetActive(false); // Quiz-End-Panel ausblenden
                Herz3D.SetActive(true); // Default 3D-Modell soll aktiv bleiben
                HerzInnenbau3D.SetActive(false); // Innenbau-3D-Modell ausblenden
                HerzQuiz3D.SetActive(false); // Quiz-3D-Modell ausblenden
                HerzAnim.SetActive(false); // 3D-Modell der Animation ausblenden
                HerzAufbau3D.SetActive(false); // Aufbau-3D-Modell ausblenden

                herzschlagSound.Stop(); // Herzschlag-Sound stoppen
                buttonClick.Play();// Button Soundeffekt abspielen
            }
            else
            {
                Default3DObject(); // Über diese Methode wird alles ausgeblendet bis auf das Default 3D-Modell
                
            }
        
    }
    // Diese Methode ist für das einblenden des Aufbau 3D-Modell zuständig
    public void HerzAufbau()
    {
        // Zunächst wird geprüft, ob das Aufbau Modell gesetzt und auch aktiv ist, wenn dies zutrifft soll der zustand beibehalten werden.
        // Wenn das Aufbau Modell nicht aktiv ist, wird der Body durchlaufen.
            if (HerzAufbau3D != null && HerzAufbau3D.activeInHierarchy == false)
            {
               if(CheckPanelOpenAllgemeineInfo()){
                    Animator animatorAll = PanelAll.GetComponent<Animator>();
                    animatorAll.SetBool("open", false);
                }
                // Hier soll das allgemeine Informationspanel ausgeblenedet werden
               if(CheckPanelOpenQuiz()){
                    Animator animator = PanelQuiz.GetComponent<Animator>();
                    animator.SetBool("open", false);
                }
                PanelQuiz.SetActive(false);
                finalQuizPanel.SetActive(false);
                Herz3D.SetActive(false);
                HerzInnenbau3D.SetActive(false);
                HerzQuiz3D.SetActive(false);
                HerzAnim.SetActive(false);
                HerzAufbau3D.SetActive(true); // Das 3D Modell des Aufbaus wird eingeblendet

                herzschlagSound.Stop();
                buttonClick.Play();
            }
            else
            {
                if(Herz3D.activeInHierarchy == false){ // Hier soll geprüft werden, ob das Default 3D-Objekt nicht gesetzt ist
                    Default3DObject(); // Sollte es nicht gesetzt sein soll alles ausgeblendet werden, bis auf das Default Modell
                }
            }
        
    }
    // Hier wird das 3D-Modell des Innenbaus eingeblendent.
    // Funktionsweise ist ähnlich zu den vorherigen Methoden.
    public void HerzInnenbau()
    {
        
            if (HerzInnenbau3D != null && HerzInnenbau3D.activeInHierarchy == false)
            {
                if(CheckPanelOpenAllgemeineInfo()){
                    Animator animatorAll = PanelAll.GetComponent<Animator>();
                    animatorAll.SetBool("open", false);
                }

                if(CheckPanelOpenQuiz()){
                    Animator animator = PanelQuiz.GetComponent<Animator>();
                    animator.SetBool("open", false);
                }
                
                PanelQuiz.SetActive(false);
                finalQuizPanel.SetActive(false);
                Herz3D.SetActive(false);
                HerzAufbau3D.SetActive(false);
                HerzQuiz3D.SetActive(false);
                HerzAnim.SetActive(false);
                HerzInnenbau3D.SetActive(true);  // Innenbau Model wird aktiviert

                herzschlagSound.Stop();
                buttonClick.Play();
                
            }
            else
            {
                if(Herz3D.activeInHierarchy == false){
                    Default3DObject();
                }
            }
        
    }
    // Bei dieser Methode wird das Quiz-Spiel gestartet
    public void HerzQuiz()
    {
            if (HerzQuiz3D != null && HerzQuiz3D.activeInHierarchy == false)
            {   
                if(CheckPanelOpenAllgemeineInfo()){
                    Animator animatorAll = PanelAll.GetComponent<Animator>();
                    animatorAll.SetBool("open", false);
                }

                if(!CheckPanelOpenQuiz()){
                    Animator animator = PanelQuiz.GetComponent<Animator>();
                    animator.SetBool("open", true); // Das Quiz-Text-Panel wird aktiviert
                }
                
                Herz3D.SetActive(false);
                HerzAufbau3D.SetActive(false);
                HerzInnenbau3D.SetActive(false);
                HerzAnim.SetActive(false);
                HerzQuiz3D.SetActive(true); // Quiz-Modell wird eingeblendet

                herzschlagSound.Stop();
                buttonClick.Play();
            }
            else
            {
                if(Herz3D.activeInHierarchy == false){
                    Default3DObject();
                }
            }
        
    }
    // In dieser Methode wird die Simulation eines Herzens abgespielt
    public void HerzAnimation()
    {
        
            if (HerzAnim != null && HerzAnim.activeInHierarchy == false)
            {
                if(CheckPanelOpenAllgemeineInfo()){
                    Animator animatorAll = PanelAll.GetComponent<Animator>();
                    animatorAll.SetBool("open", false);
                }

                if(CheckPanelOpenQuiz()){
                    Animator animator = PanelQuiz.GetComponent<Animator>();
                    animator.SetBool("open", false);
                }
                PanelQuiz.SetActive(false);
                finalQuizPanel.SetActive(false);
                Herz3D.SetActive(false);
                HerzAufbau3D.SetActive(false);
                HerzQuiz3D.SetActive(false);
                HerzInnenbau3D.SetActive(false);
                HerzAnim.SetActive(true);

                herzschlagSound.Play(); // Herzschlag Soundeffekt wird abgespielt
                buttonClick.Play();

                Anim = GetComponent<Animator>();
                Anim.speed = 0f;

                AnimStart(); // Animation-Methode wird aufgerufen
                
            }
            else
            {
                if(Herz3D.activeInHierarchy == false){
                    Default3DObject();
                   
                }
            }
        
    }
    // Hier wird die Animation der Atmung abgespielt
    private void AnimStart(){
        Anim.Play("Heart N170111_001|sub01Action", -1,0f);
        Anim.Play("sub03|sub01Action", -1,0f);
        Anim.Play("sub01|sub01Action", -1,0f);
        Anim.speed = 1f;
    }
    // Diese Methode sorgt, dass alle 3D-Modell deaktiviert werden, bis auf das Default 3D-Modell
    private void Default3DObject(){
        // Allgemeien Informationspanel ausblenden 
        if(CheckPanelOpenAllgemeineInfo()){
                    Animator animatorAll = PanelAll.GetComponent<Animator>();
                    animatorAll.SetBool("open", false);
        }
         // Quiz-Text-Panel ausblenden
        if(CheckPanelOpenQuiz()){
                Animator animator = PanelQuiz.GetComponent<Animator>();
                animator.SetBool("open", false);
        }
        herzschlagSound.Stop();// Herzschlag Sound stoppen
        buttonClick.Play();
        PanelQuiz.SetActive(false);
        finalQuizPanel.SetActive(false);
        HerzInnenbau3D.SetActive(false); // Innenbau Modell ausblenden
        HerzAufbau3D.SetActive(false); // Aufbau Modell ausblenden
        HerzQuiz3D.SetActive(false); // Quiz Modell ausblenden
        HerzAnim.SetActive(false); // Animationsmodell ausblenden
        Herz3D.SetActive(true); // Standard 3D-Modell einblenden
    }
    // Hier wird geprüft, ob das Quiz-Text-Panel aktiv ist oder nicht
    private bool CheckPanelOpenQuiz(){
        if(PanelQuiz != null)
        {
            Animator animator = PanelQuiz.GetComponent<Animator>();
            if (animator != null)
                {
                    bool isOpen = animator.GetBool("open"); // Hier wird der Zustand der Animation abgefragt
                    if(isOpen){
                        return true; // Ist der Zustand des Panels offen wird true zurückgegeben
                    }
                    else{
                        return false; // Ansonsten false
                    }
                }
                return false;
        }
        return false;
    }
    // Hier wird geprüft, ob das Allgemeine Informationspanel aktiv ist oder nicht.
    // Ähnlich zu der Methode zuvor.
    private bool CheckPanelOpenAllgemeineInfo(){
        if(PanelAll != null)
        {
            Animator animator = PanelAll.GetComponent<Animator>();
            if (animator != null)
                {
                    bool isOpen = animator.GetBool("open");
                    if(isOpen){
                        return true;
                    }
                    else{
                        return false;
                    }
                }
                return false;
        }
        return false;
    }
}
