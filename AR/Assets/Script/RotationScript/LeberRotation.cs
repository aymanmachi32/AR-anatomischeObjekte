using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
// Diese Klasse sorgt dafür, dass das alle 3D-Leber-Objekte rotieren können.
public class LeberRotation : MonoBehaviour
{
    void Update()
    {
        if (Input.touchCount == 1) // Prüft, ob eine Berührung auf dem Display vorliegt
        {
            Touch touch = Input.GetTouch(0); // Nimmt das erste Berührung aus dem Input-Array

            if (touch.phase == TouchPhase.Moved) // Prüft auf Touch-Bewegung
            {
                transform.Rotate(0f, -touch.deltaPosition.x, 0f); // Verändert nur die x-Achse, die y- und z-Achse bleiben konstant auf 0
            }
        }
    }
}
