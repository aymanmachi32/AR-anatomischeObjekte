using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
// Diese Klasse ist für das Spielen des Leber-Quiz zuständig
public class LeberQuizScript : MonoBehaviour
{
    public TextMeshProUGUI largeText;

    private List<string> list = new List<string>(); // Ein dynamsiches Array wird erstellt
    private string RandomName;

    public GameObject GallengangSpot;
    public GameObject GallenblaseSpot;
    public GameObject untereHohlveneSpot;
    public GameObject linkerLeberlappenSpot;
    public GameObject rechterLeberlappenSpot;
    public GameObject AortaSpot;
    public GameObject LeberpfortaderSpot;

    private bool GallengangSet = true;
    private bool GallenblaseSet = true;
    private bool untereHohlveneSet = true;
    private bool linkerLeberlappenSet = true;
    private bool rechterLeberlappenSet = true;
    private bool AortaSet = true;
    private bool LeberpfortaderSet = true;

    
    public GameObject TextPanel;
    public GameObject FinalTextPanel;
    public TextMeshProUGUI finalText;
    public GameObject imgWellDone;
    public GameObject imgAverage;
    public GameObject imgNotGood;
    private int counter=0; // Zählt die Anzahl an Falschen Betätigungen
    
    private float time = 0.5f; // Zeit in dem das Falsche Pin die Farbe wechseln soll
    private float repeatRate;

    public AudioSource wellDoneSound;
    public AudioSource AverageSound;
    public AudioSource NotGoodSound;

    public AudioSource CorrectPin;
    public AudioSource WrongPin;
    // Diese Methode wird einmalig bei Start des Quizes ausgeführt.
    private void Start(){
        ResetTheQuiz(); // Es wird die Methode Reset Quiz aufgerufen
    }
    // Die Update Methode wird bei jedem Frame durchlaufen
    private void Update(){
        // Hier wird geschaut, ob eine Berührung auf dem Display vorliegt
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began))
        {
        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position); // Hier wird die Position der Berührung festgelegt
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.CompareTag(GallengangSpot.tag) && GallengangSet) // Hier wird geprüft, ob der Pin des anatomische Teil angetippt wurde und ob dieser Pin schon vorher auf grün gesetzt worden ist
            {
                if(RandomName == "der Gallengang" && GallengangSet){ // Wenn das anatomisch Teil das jenige ist, welches gesuscht wurde und dies zuvor nicht dran kamm, wird der Body durchlaufen
                    GallengangSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0); // Wechseln der Frabe auf Grün
                    list.Remove(RandomName); // Anatomische Teil aus der Array List entfernen
                    GallengangSet = false; // anatomische Teil wurde einmal korrekt angetippt, dadurch kann die Farbe nicht beim erneuten tippen auf rot wechseln
                    PickRandomFromList(); // Methode zu erneuten entnahme eines Elments aus der Array List
                    CorrectPin.Play(); // Sound für die korrekte Antwort wird abgespielt
                }else{
                    GallengangSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0); // Wechseln der Frabe auf Rote
                    InvokeRepeating("ChangeColourGallengang", time, repeatRate); // Methode aufrufen die nach einer 0.5s die Farbe auf die Standard farbe zurücksetzt
                    WrongPin.Play(); // Sound für die falsche Antwort wird abgespielt
                    counter++; // Zähler wird um eins erhäht, da die Antwort falsch war
                }
            }

            if (hit.collider.CompareTag(GallenblaseSpot.tag) && GallenblaseSet)
            {
                if(RandomName == "die Gallenblase" && GallenblaseSet){
                    GallenblaseSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    GallenblaseSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    GallenblaseSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourGallenblase", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            }
            
            if (hit.collider.CompareTag(untereHohlveneSpot.tag) && untereHohlveneSet)
            {
                if(RandomName == "die untere Hohlvene" && untereHohlveneSet){
                    untereHohlveneSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    untereHohlveneSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    untereHohlveneSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColouruntereHohlvene", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            } 

            if (hit.collider.CompareTag(linkerLeberlappenSpot.tag) && linkerLeberlappenSet)
            {
                if(RandomName == "der linke Leberlappen" && linkerLeberlappenSet){
                    linkerLeberlappenSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    linkerLeberlappenSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    linkerLeberlappenSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourlinkerLeberlappen", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            }

            if (hit.collider.CompareTag(rechterLeberlappenSpot.tag) && rechterLeberlappenSet)
            {
                if(RandomName == "der rechte Leberlappen" && rechterLeberlappenSet){
                    rechterLeberlappenSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    rechterLeberlappenSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    rechterLeberlappenSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourrechterLeberlappen", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            }

            if (hit.collider.CompareTag(AortaSpot.tag) && AortaSet)
            {
                if(RandomName == "die Aorta" && AortaSet){
                    AortaSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    AortaSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    AortaSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourAorta", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            } 

            if (hit.collider.CompareTag(LeberpfortaderSpot.tag) && LeberpfortaderSet)
            {
                if(RandomName == "die Leberpfortader" && LeberpfortaderSet){
                    LeberpfortaderSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    LeberpfortaderSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    LeberpfortaderSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourLeberpfortader", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            }  
        
        }
        }
        }
    // Hier sind die Methoden, die dafür sorgen das nach 0.5s auf die Standard Farbe zurückgesetzt wird 
    private void ChangeColourGallengang()
    { 
        GallengangSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourGallenblase()
    { 
        GallenblaseSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColouruntereHohlvene()
    { 
        untereHohlveneSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourlinkerLeberlappen()
    { 
        linkerLeberlappenSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourrechterLeberlappen()
    { 
        rechterLeberlappenSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourAorta()
    { 
        AortaSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourLeberpfortader()
    { 
        LeberpfortaderSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }

    // Dieses Methode sorgt für die Beschriftung im Quiz-Panel
    public void BtnAction(){
        PickRandomFromList(); // Hier wird die PickRandomFromList aufgrufen
    } 

    // Diese Methode ist für den willkürlichen Text der eingeblendet wird
    private void PickRandomFromList(){
        string textAmAnfang = "Wo ist "; // Anfangstext für die Frage
        string textAmEnde = "?"; // Fragezeichen für das Textende
        if(list.Count == 0){ // Prüft, ob die Array List leer ist
            TextPanel.SetActive(false); // Text-Panel ausgelendet
            FinalTextPanel.SetActive(true); // End-Panel einblenden
            imgWellDone.SetActive(false);
            imgAverage.SetActive(false);
            imgNotGood.SetActive(false);
            double ergCounter = (double) 100 - counter * 7.14; // Die Punktzahl berechnen
            if(ergCounter<=0){ // Prüfen das es keine negativen Punkte gibt
                ergCounter=0;
            }
            if(ergCounter <= 100 && ergCounter >= 85.72){ // Wenn die erreichte Punktzahl zwischen 100 und 85.72 liegt
                imgWellDone.SetActive(true); // Es wird ein Bild eingeblendet, welches dem Nutzer mitteil, dass diese eine gute Leitung war
                wellDoneSound.Play(); // sehr gute Leistung Sound wird abgespielt
                finalText.text = "Gut gemacht. Weiter So!!!. Du hast " + ergCounter + "% erreicht."; // Text wird eingeblenden mit den erreichten Punkte
            }else{
                if(ergCounter <= 85.6 && ergCounter >= 57.16){ // Wenn die erreichte Punktzahl zwischen 85.6 und 57.16 liegt
                    imgAverage.SetActive(true); // Es wird ein Bild eingeblendet, welches dem Nutzer mitteil, dass diese eine durchschnittliche Leitung war
                    AverageSound.Play(); // durchschnittliche Leistung Sound wird abgespielt
                    finalText.text = "Übe noch ein bisschen. Du hast " + ergCounter + "% erreicht."; // Text wird eingeblenden mit den erreichten Punkte
                }else{ // Wenn die erreichte Punktzahl unterhalb von 57.16 liegt
                    imgNotGood.SetActive(true); // Es wird ein Bild eingeblendet, welches dem Nutzer mitteil, dass diese eine schlechte Leitung war
                    NotGoodSound.Play(); // schlechte Leistung Sound wird abgespielt
                    finalText.text = "Übeung mach den Meister. Du hast " + ergCounter + "% erreicht."; // Text wird eingeblenden mit den erreichten Punkte
                }
            }
            
        }else{
            RandomName = list[Random.Range(0, list.Count)]; // Aus der Array List wird ein willkürliches anatomische Teil gewählt
            largeText.text = textAmAnfang + RandomName + textAmEnde; // Hier wird dann der Text dem den Nutzer eingebelendet wird
        }
    }
    // Diese Methode wird bei Start ausgeführt, damit alle Einstellungen zurückgesetzt werden können
    public void ResetTheQuiz(){
        wellDoneSound.Stop(); // Sehr Gute Leistung Sound abstellen, falls dies aktiv ist
        AverageSound.Stop(); // Durchschnittliche Leistung Sound abstellen, falls dies aktiv ist
        NotGoodSound.Stop(); // nicht gute Leistung Sound abstellen, falls dies aktiv ist
        FinalTextPanel.SetActive(false); // Final Panel ausblenden, falls dies aktiv ist
        TextPanel.SetActive(true); // Text-Panel einblenden
        list.Clear(); // Array List leeren, falls Spiel abgebrochen wird
        counter = 0; // Zähler zurücksetzten
        
        list.Add("der Gallengang"); // Befüllung der anatmoischen Teile in die Array List
        list.Add("die Gallenblase");
        list.Add("die untere Hohlvene");
        list.Add("der linke Leberlappen");
        list.Add("der rechte Leberlappen");
        list.Add("die Aorta");
        list.Add("die Leberpfortader");


        GallengangSet = true; // Ist für die Überprüfung, ob ein Pin schonmal Korrekt bestimmt wurde
        GallenblaseSet = true;
        untereHohlveneSet = true;
        linkerLeberlappenSet = true;
        rechterLeberlappenSet = true;
        AortaSet = true;
        LeberpfortaderSet = true;


        GallengangSpot.GetComponent<Renderer>().material.color = new Color(250,250,250); // Farben der Pins auf weiß zurücksetzten
        GallenblaseSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        untereHohlveneSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        linkerLeberlappenSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        rechterLeberlappenSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        AortaSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        LeberpfortaderSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
    }
}
