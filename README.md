# **AR-Anatomie** 
<p align="center"> <img width="180" height="180" src="/AR/Assets/Logo/Logo.jpg"></p>

## Was ist AR-Anatomie?
Bei **AR-Anatomie** handelt es sich um eine Learning-App, welches Augmented Reality (kurz AR) Technologie nutzt. Diese Anwendung soll Biologie-Schülern anatomische Objekte besser visualisiert werden. Über eine optische Darstellung der anatomischen Objekte kann der Schüler mehr Informationen aufnehmen als über ein Text. Aus diesem Grund wird das Herz, die Lunge, die Leber und das Gehirn über sogenannte "_ImageTargets_" in der echten Welt projiziert. Darüber hinaus kann der Schüler über sein Android-Smartphone mit den entsprechenden 3D-Modellen interagieren. Dem Schüler stehen verschieden Modi zur Verfügung, indem dieser sich intensiv mit den anatomischen Objekten beschäftigen kann.

## Welche Modi gibt es?
**AR-Anatomie** verfügt über fünf unterschiedliche Modi:
-  Im **Allgemeine Information** Modus werden allgemeine Informationen über das anatomische Objekt wie z.B. die Funktionsweise erläutert.
-  Im **Aufbau** Modus wird der Aufbau des anatomischen Objektes in einem 3D-Modell visualisiert.
-  Im **Quiz** Modus kann der Nutzer sein erlerntes Wissen einsetzten, indem dieser zum Aufbau abgefragt wird.
-  Im **Innenbau** Modus kann der Nutzer den Aufbau des Inneren eines anatomischen Objektes mittels eines weiteren 3D-Modell bewundern.
-  Im **Animation** Modus wird die Funktion, das anatomische Objekt simuliert.

## Installationsanleitung 

1. Damit Sie die Anwendung auf Ihr Android-Smartphone übertragen können, laden Sie bitte hierfür die _**AppARAnatomie.apk**_ herunter.
2. Anschließend verbinden Sie Ihr Smartphone mit Ihrem Rechner.
3. Nun muss Ihr Android-Smartphone unter "Geräte und Laufwerke" zu sehen sein.
4. Fügen Sie in dem Download Ordner Ihres Smartphones die _**AppARAnatomie.apk**_ ein.

Nun befindet sich die Anwendung auf Ihr Android-Smartphone.

5. Um nun die Anwendung auf Ihr Smartphone zum Laufen zu bringen, gehen Sie hierfür auf Ihr Download Ordner in Ihrem Smartphone.
6. Dort sollte die Anwendung mit dem Logo zu sehen sein. Tippen Sie auf die Learning-App.
7. Installieren Sie die Anwendung über den _Package installer_ .

## Troubleshooting
Sollte Ihr "Eigene Dateien"-Manager aus Sicherheitsgründen die Installation von unbekannten Apps nicht durchführen. 
   - Gehen Sie auf **Einstellungen** > **Unbekannte Apps installieren** > aktivieren Sie **Eigene Dateien** 
   - Führen Sie die Schritte 5-7 erneut aus.
