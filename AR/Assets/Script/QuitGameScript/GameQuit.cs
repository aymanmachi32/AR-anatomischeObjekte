using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Diese Klasse ist dafür zuständig die Anwendung zu verlassen
public class GameQuit : MonoBehaviour
{
   
    public void ExitGame()
    {
        #if UNITY_EDITOR // Prüft, ob die Anwendung über den Unity-Editor läuft
            UnityEditor.EditorApplication.isPlaying = false; // Beendet alles
        #endif
        Application.Quit(); // Verlässt die Anwendung
    }
}
