using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
// Diese Klasse ist für das Öffenen des Hauptmenüs im Gehirn Bereich zuständig
public class OpenMenuGehirn : MonoBehaviour
{
    public GameObject Panel;
    private bool isOpen = false;

    public AudioSource MenuSound;

    public void OpenMenuButtonGehirn()
    {
        if (Panel != null)
        {
            Animator animator = Panel.GetComponent<Animator>(); // Hier wird die Animation in einer Variable gespeichert
            if (animator != null)
            {
                isOpen = animator.GetBool("open"); // Der Zustand wird Animation wird hier abgefragt
                animator.SetBool("open", !isOpen); // Anschließend wird das Komplement des Zustand gesetzt
                if (!isOpen) // Hier soll der Text angezeigt werden, den das Menü animt beim erneuten an tippen
                {
                    GameObject.Find("GehirnMenuButton>>").GetComponentInChildren<TextMeshProUGUI>().text = "<<"; // Menü ist offen und soll bein erneuten tippen geschlossen werden
                    isOpen = true;
                    MenuSound.Play();

                }
                else
                {
                    GameObject.Find("GehirnMenuButton>>").GetComponentInChildren<TextMeshProUGUI>().text = ">>"; // Menü ist geschlossen und soll bein erneuten tippen geöffnet werden
                    isOpen = false;
                    MenuSound.Play();
                }
            }

        }
    }
}
