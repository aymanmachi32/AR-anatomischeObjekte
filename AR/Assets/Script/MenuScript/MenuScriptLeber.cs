using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
// Diese Klasse ist für das wechseln der Modi im Hauptmenü der Leber zuständig
public class MenuScriptLeber : MonoBehaviour
{
    public GameObject PanelQuiz;
    public GameObject PanelAll;
    public GameObject Leber3D;
    public GameObject LeberAufbau3D;
    public GameObject LeberInnenbau3D;
    public GameObject LeberQuiz3D;

    
    public GameObject finalQuizPanel;
    public AudioSource buttonClick;
    // Über diese Methode werden die Allgemeinen Informationen eingeblendet
    public void LeberAllgemeineInfo()
    {
            // Hier wird Quiz-Text-Panel ausgeblendet, wenn dieser Aktiv ist
            if(CheckPanelOpenQuiz()){
                Animator animator = PanelQuiz.GetComponent<Animator>();
                animator.SetBool("open", false);
            
            }
            // Wenn das allgemeine Informationspanel, nicht offen ist soll die Animation abgespielt werden
            if (!CheckPanelOpenAllgemeineInfo())
            {
                
                Animator animatorAll = PanelAll.GetComponent<Animator>();
                animatorAll.SetBool("open", true); // Animation abspielen
                PanelQuiz.SetActive(false); // Quiz-Panel ausblenden
                finalQuizPanel.SetActive(false); // Quiz-End-Panel ausblenden
                Leber3D.SetActive(true); // Default 3D-Modell soll aktiv bleiben
                LeberInnenbau3D.SetActive(false); // Innenbau-3D-Modell ausblenden
                LeberQuiz3D.SetActive(false); // Quiz-3D-Modell ausblenden
                LeberAufbau3D.SetActive(false); // Aufbau-3D-Modell ausblenden

                buttonClick.Play(); // Button Soundeffekt abspielen
            }
            else
            {
                Default3DObject(); // Über diese Methode wird alles ausgeblendet bis auf das Default 3D-Modell
                
            }
        
    }
    // Diese Methode ist für das einblenden des Aufbau 3D-Modell zuständig
    public void LeberAufbau()
    {
        // Zunächst wird geprüft, ob das Aufbau Modell gesetzt und auch aktiv ist, wenn dies zutrifft soll der zustand beibehalten werden.
        // Wenn das Aufbau Modell nicht aktiv ist, wird der Body durchlaufen.
        if (LeberAufbau3D != null && LeberAufbau3D.activeInHierarchy == false)
        {
            if(CheckPanelOpenAllgemeineInfo()){
                    Animator animatorAll = PanelAll.GetComponent<Animator>();
                    animatorAll.SetBool("open", false);
            }
            // Hier soll das allgemeine Informationspanel ausgeblenedet werden
            if(CheckPanelOpenQuiz()){
                    Animator animator = PanelQuiz.GetComponent<Animator>();
                    animator.SetBool("open", false);
            }
            PanelQuiz.SetActive(false);
            finalQuizPanel.SetActive(false);
            Leber3D.SetActive(false);
            LeberInnenbau3D.SetActive(false);
            LeberQuiz3D.SetActive(false);
            LeberAufbau3D.SetActive(true); // Das 3D Modell des Aufbaus wird eingeblendet

            buttonClick.Play();
        }
        else
        {
            if(Leber3D.activeInHierarchy == false){ // Hier soll geprüft werden, ob das Default 3D-Objekt nicht gesetzt ist
                    Default3DObject();  // Sollte es nicht gesetzt sein soll alles ausgeblendet werden, bis auf das Default Modell
             }
        }
    }
    // Hier wird das 3D-Modell des Innenbaus eingeblendent.
    // Funktionsweise ist ähnlich zu den vorherigen Methoden
    public void LeberInnenbau()
    {
        if (LeberInnenbau3D != null && LeberInnenbau3D.activeInHierarchy == false)
        {
            if(CheckPanelOpenAllgemeineInfo()){
                    Animator animatorAll = PanelAll.GetComponent<Animator>();
                    animatorAll.SetBool("open", false);
            }
            if(CheckPanelOpenQuiz()){
                    Animator animator = PanelQuiz.GetComponent<Animator>();
                    animator.SetBool("open", false);
            }
            PanelQuiz.SetActive(false);
            finalQuizPanel.SetActive(false);
            Leber3D.SetActive(false);
            LeberAufbau3D.SetActive(false);
            LeberQuiz3D.SetActive(false);
            LeberInnenbau3D.SetActive(true); // Innenbau Model wird aktiviert

            buttonClick.Play();
            
        }
        else
        {
            if(Leber3D.activeInHierarchy == false){
                    Default3DObject(); 
             }
           
        }
    }
    // Bei dieser Methode wird das Quiz-Spiel gestartet
    public void LeberQuiz()
    {
        if (LeberQuiz3D != null && LeberQuiz3D.activeInHierarchy == false)
        {
            if(CheckPanelOpenAllgemeineInfo()){
                    Animator animatorAll = PanelAll.GetComponent<Animator>();
                    animatorAll.SetBool("open", false);
            }
            if(!CheckPanelOpenQuiz()){
                    Animator animator = PanelQuiz.GetComponent<Animator>();
                    animator.SetBool("open", true); // Das Quiz-Text-Panel wird aktiviert
            }
            Leber3D.SetActive(false);
            LeberAufbau3D.SetActive(false);
            LeberInnenbau3D.SetActive(false);
            LeberQuiz3D.SetActive(true); // Quiz-Modell wird eingeblendet

            buttonClick.Play();
        }
        else
        {
            if(Leber3D.activeInHierarchy == false){
                    Default3DObject(); 
             }
        }
    }
    // Diese Methode sorgt, dass alle 3D-Modell deaktiviert werden, bis auf das Default 3D-Modell
    private void Default3DObject(){
        // Allgemeien Informationspanel ausblenden 
        if(CheckPanelOpenAllgemeineInfo()){
                    Animator animatorAll = PanelAll.GetComponent<Animator>();
                    animatorAll.SetBool("open", false);
        }
        // Quiz-Text-Panel ausblenden 
        if(CheckPanelOpenQuiz()){
                Animator animator = PanelQuiz.GetComponent<Animator>();
                animator.SetBool("open", false);
        }
        buttonClick.Play(); // Ateme Sound stoppen
        PanelQuiz.SetActive(false);
        finalQuizPanel.SetActive(false);
        LeberInnenbau3D.SetActive(false); // Innenbau Modell ausblenden
        LeberAufbau3D.SetActive(false); // Aufbau Modell ausblenden
        LeberQuiz3D.SetActive(false); // Quiz Modell ausblenden
        Leber3D.SetActive(true); // Standard 3D-Modell einblenden
    }
    // Hier wird geprüft, ob das Quiz-Text-Panel aktiv ist oder nicht
    private bool CheckPanelOpenQuiz(){
        if(PanelQuiz != null)
        {
            Animator animator = PanelQuiz.GetComponent<Animator>();
            if (animator != null)
                {
                    bool isOpen = animator.GetBool("open"); // Hier wird der Zustand der Animation abgefragt
                    if(isOpen){
                        return true; // Ist der Zustand des Panels offen wird true zurückgegeben
                    }
                    else{
                        return false; // Ansonsten false
                    }
                }
                return false;
        }
        return false;
    }
    // Hier wird geprüft, ob das Allgemeine Informationspanel aktiv ist oder nicht.
    // Ähnlich zu der Methode zuvor.
    private bool CheckPanelOpenAllgemeineInfo(){
        if(PanelAll != null)
        {
            Animator animator = PanelAll.GetComponent<Animator>();
            if (animator != null)
                {
                    bool isOpen = animator.GetBool("open");
                    if(isOpen){
                        return true;
                    }
                    else{
                        return false;
                    }
                }
                return false;
        }
        return false;
    }
}
