using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// Diese Klasse ist für das Spielen des Herz-Quiz zuständig
public class HerzQuizScript : MonoBehaviour
{
    public TextMeshProUGUI largeText;

    private List<string> list = new List<string>(); // Ein dynamsiches Array wird erstellt
    private string RandomName;

    public GameObject TextPanel;
    public GameObject FinalTextPanel;
    public TextMeshProUGUI finalText;
    public GameObject imgWellDone;
    public GameObject imgAverage;
    public GameObject imgNotGood;


    public GameObject AortaSpot;
    public GameObject LungenschlagaderSpot;
    public GameObject LungenveneSpot;
    public GameObject linkeVorhofSpot;
    public GameObject linkeKoronararterieSpot;
    public GameObject obereHohlveneSpot;
    public GameObject rechteVorhofSpot;
    public GameObject rechteKoronararterieSpot;

    private bool AortaSet = true;
    private bool LungenschlagaderSet = true;
    private bool LungenveneSet = true;
    private bool linkeVorhofSet = true;
    private bool linkeKoronararterieSet = true;
    private bool obereHohlveneSet = true;
    private bool rechteVorhofSet = true;
    private bool rechteKoronararterieSet = true;


    private float time = 0.5f; // Zeit in dem das Falsche Pin die Farbe wechseln soll
    private float repeatRate;
    
    private int counter=0; // Zählt die Anzahl an Falschen Betätigungen

    public AudioSource wellDoneSound;
    public AudioSource AverageSound;
    public AudioSource NotGoodSound;

    public AudioSource CorrectPin;
    public AudioSource WrongPin;

// Diese Methode wird einmalig bei Start des Quizes ausgeführt.
    private void Start(){
        wellDoneSound.Stop(); // Sehr Gute Leistung Sound abstellen, falls dies aktiv ist
        AverageSound.Stop(); // Durchschnittliche Leistung Sound abstellen, falls dies aktiv ist
        NotGoodSound.Stop(); // nicht gute Leistung Sound abstellen, falls dies aktiv ist
        FinalTextPanel.SetActive(false); // Final Panel ausblenden, falls dies aktiv ist
        TextPanel.SetActive(true); // Text-Panel einblenden
        list.Clear(); // Array List leeren, falls Spiel abgebrochen wird
        counter = 0; // Zähler zurücksetzten

        list.Add("die Aorta");  // Befüllung der anatmoischen Teile in die Array List
        list.Add("die Lungenschlagader");
        list.Add("die Lungenvene");
        list.Add("der linke Vorhof");
        list.Add("die linke Koronararterie");
        list.Add("die obere Hohlvene");
        list.Add("der rechte Vorhof");
        list.Add("die rechte Koronararterie");

        AortaSet = true;              // Ist für die Überprüfung, ob ein Pin schonmal Korrekt bestimmt wurde
        LungenschlagaderSet = true;
        LungenveneSet = true;
        linkeVorhofSet = true;
        linkeKoronararterieSet = true;
        obereHohlveneSet = true;
        rechteVorhofSet = true;
        rechteKoronararterieSet = true;

        AortaSpot.GetComponent<Renderer>().material.color = new Color(250,250,250); // Farben der Pins auf weiß zurücksetzten
        LungenschlagaderSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        LungenveneSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        linkeVorhofSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        linkeKoronararterieSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        obereHohlveneSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        rechteVorhofSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
        rechteKoronararterieSpot.GetComponent<Renderer>().material.color = new Color(250,250,250);
    }

    // Die Update Methode wird bei jedem Frame durchlaufen
    private void Update(){
        // Hier wird geschaut, ob eine Berührung auf dem Display vorliegt
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began))
        {
        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position); // Hier wird die Position der Berührung festgelegt
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.CompareTag(AortaSpot.tag) && AortaSet) // Hier wird geprüft, ob der Pin des anatomische Teil angetippt wurde und ob dieser Pin schon vorher auf grün gesetzt worden ist
            {
                if(RandomName == "die Aorta" && AortaSet){ // Wenn das anatomisch Teil das jenige ist, welches gesuscht wurde und dies zuvor nicht dran kamm, wird der Body durchlaufen
                    AortaSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0); // Wechseln der Frabe auf Grün
                    list.Remove(RandomName); // Anatomische Teil aus der Array List entfernen
                    AortaSet = false; // anatomische Teil wurde einmal korrekt angetippt, dadurch kann die Farbe nicht beim erneuten tippen auf rot wechseln
                    PickRandomFromList(); // Methode zu erneuten entnahme eines Elments aus der Array List
                    CorrectPin.Play(); // Sound für die korrekte Antwort wird abgespielt
                }else{
                    AortaSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0); // Wechseln der Frabe auf Rote
                    InvokeRepeating("ChangeColourAorta", time, repeatRate); // Methode aufrufen die nach einer 0.5s die Farbe auf die Standard farbe zurücksetzt
                    WrongPin.Play(); // Sound für die falsche Antwort wird abgespielt
                    counter++; // Zähler wird um eins erhäht, da die Antwort falsch war
                    
                }
            }

            if (hit.collider.CompareTag(LungenschlagaderSpot.tag) && LungenschlagaderSet)
            {
                if(RandomName == "die Lungenschlagader" && LungenschlagaderSet){
                    LungenschlagaderSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    LungenschlagaderSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    LungenschlagaderSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourLungenschlagader", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            }
            
            if (hit.collider.CompareTag(LungenveneSpot.tag) && LungenveneSet)
            {
                if(RandomName == "die Lungenvene" && LungenveneSet){
                    LungenveneSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    LungenveneSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    LungenveneSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourLungenvene", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            } 

            if (hit.collider.CompareTag(linkeVorhofSpot.tag) && linkeVorhofSet)
            {
                if(RandomName == "der linke Vorhof" && linkeVorhofSet){
                    linkeVorhofSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    linkeVorhofSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    linkeVorhofSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourlinkerVorhof", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            }

            if (hit.collider.CompareTag(linkeKoronararterieSpot.tag) && linkeKoronararterieSet)
            {
                if(RandomName == "die linke Koronararterie" && linkeKoronararterieSet){
                    linkeKoronararterieSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    linkeKoronararterieSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    linkeKoronararterieSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourlinkeKoronararterie", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            } 
            
            if (hit.collider.CompareTag(obereHohlveneSpot.tag) && obereHohlveneSet)
            {
                if(RandomName == "die obere Hohlvene" && obereHohlveneSet){
                    obereHohlveneSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    obereHohlveneSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    obereHohlveneSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourObereHohlvene", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            }

            if (hit.collider.CompareTag(rechteVorhofSpot.tag) && rechteVorhofSet)
            {
                if(RandomName == "der rechte Vorhof" && rechteVorhofSet){
                    rechteVorhofSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    rechteVorhofSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    rechteVorhofSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourrechterVorhof", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            } 
             if (hit.collider.CompareTag(rechteKoronararterieSpot.tag) && rechteKoronararterieSet)
            {
                if(RandomName == "die rechte Koronararterie" && rechteKoronararterieSet){
                    rechteKoronararterieSpot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                    list.Remove(RandomName);
                    rechteKoronararterieSet = false;
                    PickRandomFromList();
                    CorrectPin.Play();
                }else{
                    rechteKoronararterieSpot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
                    InvokeRepeating("ChangeColourrechteKoronararterie", time, repeatRate);
                    WrongPin.Play();
                    counter++;
                }
            }
        }
        }
        }
    // Hier sind die Methoden, die dafür sorgen das nach 0.5s auf die Standard Farbe zurückgesetzt wird
    private void ChangeColourAorta()
    { 
        AortaSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourLungenschlagader()
    { 
        LungenschlagaderSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourLungenvene()
    { 
        LungenveneSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourlinkerVorhof()
    { 
        linkeVorhofSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourlinkeKoronararterie()
    { 
        linkeKoronararterieSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourObereHohlvene()
    { 
        obereHohlveneSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourrechterVorhof()
    { 
        rechteVorhofSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }
    private void ChangeColourrechteKoronararterie()
    { 
        rechteKoronararterieSpot.GetComponent<Renderer>().material.color = new Color(255,131,131);
    }

    // Dieses Methode sorgt für die Beschriftung im Quiz-Panel
    public void BtnAction(){
        PickRandomFromList(); // Hier wird die PickRandomFromList aufgrufen
    } 

    // Diese Methode ist für den willkürlichen Text der eingeblendet wird
    private void PickRandomFromList(){
        string textAmAnfang = "Wo ist "; // Anfangstext für die Frage
        string textAmEnde = "?"; // Fragezeichen für das Textende
        if(list.Count == 0){ // Prüft, ob die Array List leer ist
            TextPanel.SetActive(false); // Text-Panel ausgelendet
            FinalTextPanel.SetActive(true); // End-Panel einblenden
            imgWellDone.SetActive(false);
            imgAverage.SetActive(false);
            imgNotGood.SetActive(false);
            double ergCounter = (double) 100 - counter*6.25; // Die Punktzahl berechnen
            if(ergCounter<=0){ // Prüfen das es keine negativen Punkte gibt
                ergCounter=0;
            }
            if(ergCounter <= 100 && ergCounter >= 87.5){ // Wenn die erreichte Punktzahl zwischen 100 und 87.5 liegt
                imgWellDone.SetActive(true); // Es wird ein Bild eingeblendet, welches dem Nutzer mitteil, dass diese eine gute Leitung war
                wellDoneSound.Play(); // sehr gute Leistung Sound wird abgespielt
                finalText.text = "Gut gemacht. Weiter So!!!. Du hast " + ergCounter + "% erreicht."; // Text wird eingeblenden mit den erreichten Punkte
            }else{
                if(ergCounter <= 86 && ergCounter >= 68.75){ // Wenn die erreichte Punktzahl zwischen 86 und 68.75 liegt
                    imgAverage.SetActive(true); // Es wird ein Bild eingeblendet, welches dem Nutzer mitteil, dass diese eine durchschnittliche Leitung war
                    AverageSound.Play(); // durchschnittliche Leistung Sound wird abgespielt
                    finalText.text = "Übe noch ein bisschen. Du hast " + ergCounter + "% erreicht."; // Text wird eingeblenden mit den erreichten Punkte
                }else{  // Wenn die erreichte Punktzahl unterhalb von 68.75 liegt
                    imgNotGood.SetActive(true);  // Es wird ein Bild eingeblendet, welches dem Nutzer mitteil, dass diese eine schlechte Leitung war
                    NotGoodSound.Play(); // schlechte Leistung Sound wird abgespielt
                    finalText.text = "Übeung mach den Meister. Du hast " + ergCounter + "% erreicht."; // Text wird eingeblenden mit den erreichten Punkte
                }
            }
            
        }else{
            RandomName = list[Random.Range(0, list.Count)]; // Aus der Array List wird ein willkürliches anatomische Teil gewählt
            largeText.text = textAmAnfang + RandomName + textAmEnde; // Hier wird dann der Text dem den Nutzer eingebelendet wird
        }
    }
    // Diese Methode ist für das Reseten zuständig
    public void ResetTheQuiz(){
        Start(); // Es wird die Start-Methode aufgerufen
    }
}

