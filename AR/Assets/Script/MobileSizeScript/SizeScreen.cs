using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Diese Klasse ist für das dynamische Resizing verantwortlich
public class SizeScreen : MonoBehaviour
{
     void Start()
    {
        // Wechseln von 1280 x 720 Full-Screen bei 60 hz
        Screen.SetResolution(1280, 720, FullScreenMode.ExclusiveFullScreen, 60);
    }
}
